# Subsurface Test: 1D Infiltration

This is the Warrick's 1D infiltration problem with analytical solution available (see for example, Li etal., JoH2020, https://doi.org/10.1016/j.jhydrol.2020.125809).
